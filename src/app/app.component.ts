import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DynamicFieldDTO } from './models/dynamicFieldDTO';
import { FieldsControlService } from './services/fields-control.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  formBuilder: FormGroup;
  form: FormGroup;
  result = '';
  fields: DynamicFieldDTO[];
  helper = '{"username":{"value":"Nareman","key":"userName","label":"Username","required":"true","controlType":"textbox","type":"text" },"phone":{"value":0,"key":"phone","label":"PhoneNumber","controlType":"textbox","type":"text"},"paymentMethod":{"value":"","key":"paymentMethod","label":"Payment Method","required":"true","controlType":"dropdown","type":"text", "options":[ {"key": 1, "value": "Cash"}, {"key": 2, "value": "Online"} ]},"gender":{"value":0,"key":"gender","label":"Gender","required":"false","controlType":"radioButton","type":"", "options":[ {"key": 1, "value": "Female"}, {"key": 2, "value": "Male"} ]},"choose yours":{"key":"yourInterests","controlType":"checkbox","type": "multi", "label": "Your Interests", "options":[ {"key": 1, "value": "Football"}, {"key": 2, "value": "shopping"}] },"rememberme":{"value":"1","key":"rememberme","required":"false","controlType":"checkbox", "label": "Remember me", "options":[ {"key": 1, "value": "Remember me"}] },"save":{"value":"save","controlType":"button","type":"submit" },"cancel":{"value":"cancel","controlType":"button","type":"button" }}';
    constructor(private fieldControlService: FieldsControlService) {
    this.formBuilder = new FormGroup({
      formFields: new FormControl('', [Validators.required])
    });
    this.form = new FormGroup({
    });
  }

  onFormBuilderSubmit() {
    let fieldsJsonObj = JSON.parse(this.formBuilder.value.formFields);
    let arr = [];
    
    for(let key in fieldsJsonObj){  
     if(fieldsJsonObj.hasOwnProperty(key)){  
      arr.push(fieldsJsonObj[key]);  
     }  
    }
    this.fields = arr;
    console.log(this.fields);
    
    this.form = this.fieldControlService.toFormGroup(this.fields);
    console.log(this.form);
    
  }

  onSubmit() {
    this.result = JSON.stringify(this.form.value);
    this.form.reset();
  }


}
