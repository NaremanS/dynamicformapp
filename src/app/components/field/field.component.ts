import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DynamicFieldDTO } from 'src/app/models/dynamicFieldDTO';
import { Router } from '@angular/router';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit {

  @Input() field: DynamicFieldDTO;
  @Input() form: FormGroup;
  get isValid() { return this.form.controls[this.field.key]?.valid; }
  constructor(private router: Router) { }
  
  ngOnInit() {
  }

  cancel(){
    window.location.reload();
  }

}
