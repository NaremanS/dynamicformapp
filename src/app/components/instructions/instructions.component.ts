import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.scss']
})
export class InstructionsComponent implements OnInit {

  fieldDTO = `"FieldName":{ 
    "value": "string";
    "key": "string";
    "label"?: "string";
    "required"?: "boolean";
    "controlType": "string";
    "type"?: "string";
    "options"?: DdlOptions[]
  }`
  constructor() { }

  ngOnInit() {
  }

}
