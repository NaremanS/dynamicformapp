import { DynamicFieldDTO } from './dynamicFieldDTO';

export class DynamicFieldDdlDTO extends DynamicFieldDTO {
    controlType = 'dropdown';
    options: {key: string, value: string}[];
}
