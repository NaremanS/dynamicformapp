import { DdlOptions } from './DdlOptions';

export class DynamicFieldDTO {
    value: string;
    key: string;
    label?: string;
    required?: boolean;
    controlType: string;
    type?: string;
    options?: DdlOptions[]
}
