/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FieldsControlService } from './fields-control.service';

describe('Service: FieldsControl', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FieldsControlService]
    });
  });

  it('should ...', inject([FieldsControlService], (service: FieldsControlService) => {
    expect(service).toBeTruthy();
  }));
});
