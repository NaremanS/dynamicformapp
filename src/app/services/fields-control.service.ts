import { Injectable } from '@angular/core';
import { DynamicFieldDTO } from '../models/dynamicFieldDTO';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FieldsControlService {

  constructor() { }
  toFormGroup(questions: DynamicFieldDTO[]) {
    const group: any = {};

    questions.forEach(question => {
      if(question.controlType != 'button'){
        // group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
        //   : new FormControl(question.value || '');
        if(question.controlType == 'checkbox'){
          question.options.forEach(chx => {
            group[chx.value] = this.addFromControl(chx.value);
          })
        }else  group[question.key] = this.addFromControl(question);
      }
    });
    return new FormGroup(group);
  }

  addFromControl(field){
    const formGroup: any = {};
    return field.required ? new FormControl(field.value || '', Validators.required)
    : new FormControl(field.value || '');
  }
}
